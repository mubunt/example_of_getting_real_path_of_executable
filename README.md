 # *example_of_getting_real_path_of_executable*, Example of how to get the real path name of the current executable.

## LICENSE
**example_of_getting_real_path_of_executable** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE
``` bash
$ ./linux/example_of_getting_real_path_of_executable 
Real path of ./linux/example_of_getting_real_path_of_executable is /home/michel/Sandbox/GitLab/example_of_getting_real_path_of_executable/linux/example_of_getting_real_path_of_executable.
$ sudo cp ./linux/example_of_getting_real_path_of_executable /usr/bin
[sudo] Mot de passe de michel : 
$ example_of_getting_real_path_of_executable
Real path of example_of_getting_real_path_of_executable is /usr/bin/example_of_getting_real_path_of_executable.
$ sudo rm /usr/bin/example_of_getting_real_path_of_executable 
$ 

```
## STRUCTURE OF THE APPLICATION
This section walks you through **example_of_getting_real_path_of_executable**'s structure. Once you understand this structure, you will easily find your way around in **example_of_getting_real_path_of_executable**'s code base.

``` bash
$ yaTree
../                                                   # Application level
├── src/                                             # Source directory
│   ├── Makefile                                     # Makefile
│   └── example_of_getting_real_path_of_executable.c # C source of example
├── COPYING.md                                       # GNU General Public License markdown file
├── LICENSE.md                                       # License markdown file
├── Makefile                                         # Makefile
├── README.md                                        # ReadMe markdown file
├── RELEASENOTES.md                                  # Release Notes markdown file
└── VERSION                                          # Version identification text file

1 directories, 8 files
$ 
```
## HOW TO BUILD THIS APPLICATION
```Shell
$ cd example_of_getting_real_path_of_executable
$ make clean all
```
## SOFTWARE REQUIREMENTS
- For usage, nothing particular...
- Developped and tested on XUBUNTU 22.04, GCC version 11.2.0 (Ubuntu 11.2.0-19ubuntu1), GNU Make 4.3

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***