//------------------------------------------------------------------------------
// Copyright (c) 2022, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: example_of_getting_real_path_of_executable
// Example of how to get the real path name of the current executable
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#ifndef LINUX
#include <windows.h>
#include <winbase.h>
#endif
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#ifdef LINUX
#define ProcessPseudoFileSystem	"/proc/self/exe"
#endif
#define error(fmt, ...) 		do { fprintf(stderr, "\nERROR: " fmt "\n\n", __VA_ARGS__); } while (0)
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	char realPath[PATH_MAX];
	//----  Go on --------------------------------------------------------------
#ifdef LINUX
	//----- Get real path of this executable to compute, for example, associate data files
	// /proc/self is a symbolic link to the process-ID subdir
	// of /proc, e.g. /proc/4323 when the pid of the process
	// of this program is 4323.
	// Inside /proc/<pid> there is a symbolic link to the
	// executable that is running as this <pid>.  This symbolic
	// link is called "exe".
	// So if we read the path where the symlink /proc/self/exe
	// points to we have the full path of the executable.
	if (readlink(ProcessPseudoFileSystem, realPath, sizeof(realPath)) == -1) {
		error("%s", "Cannot get path of the current executable.");
		return EXIT_FAILURE;
	}
#else
	if (0 == GetModuleFileName(NULL, realPath, sizeof(realPath))) {
		error("%s", "Cannot get path of the current executable.");
		return EXIT_FAILURE;
	}
#endif
	fprintf(stdout, "Real path of ""%s"" is ""%s"".\n", argv[0], realPath);
	//---- Exit ----------------------------------------------------------------
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
