# RELEASE NOTES: *example_of_getting_real_path_of_executable*, Example of how to get the real path name of the current executable.

Functional limitations, if any, of this version are described in the *README.md* file.

**Version 1.0.1**:
  - Removed unused make file.

**Version 1.0.0**:
  - First version.
