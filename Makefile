#------------------------------------------------------------------------------
# Copyright (c) 2022, Michel RIZZO.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# Project: example_of_getting_real_path_of_executable
# Example of how to get the real path name of the current executable
#-------------------------------------------------------------------------------
COPYRIGHT   = "2022"
DIRS        = src

include .make/topLevel.mk
#-------------------------------------------------------------------------------

